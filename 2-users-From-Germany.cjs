
const userData = require(`./1-users.cjs`);

const germanUsers = Object.entries(userData).reduce( (accumulator,current) =>{

   if(current[1]["nationality"] === "Germany"){

    accumulator.push(current[0]);
   }
   return accumulator

},[]);

console.log(germanUsers);