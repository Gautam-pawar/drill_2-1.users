
const userData = require(`./1-users.cjs`);

const Gamers = Object.entries(userData).reduce((acc, obj) => {

    if (obj[1]["interests"][0].includes("Video Games")) {

        acc.push(obj[0]);
    }

    return acc

}, []);

console.log(Gamers);